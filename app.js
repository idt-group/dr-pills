let express = require("express")


let app = express()


app.get(
    "/",
    (_, res) =>
    {
        res.sendFile(__dirname + "/index.html")
    }
)
app.get(
    "/:file_name",
    (req, res) =>
    {
        res.sendFile(`${__dirname}/${req.params.file_name}`)
    }
)

app.listen(5000)
